/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_cmpchars.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/02/28 20:28:07 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/16 17:07:30 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

static void	ft_cmpchar(t_printf *lst)
{
	int		i;

	i = 0;
	lst->plen = (lst->width > 1 ? lst->width : 1);
	if (!(lst->print = ft_strcnew(lst->plen, ' ')))
		return ;
	if (lst->flag[4] && !lst->flag[1])
		while (lst->print[i + 1])
			lst->print[i++] = '0';
	lst->print[(lst->flag[1] ? 0 : lst->plen - 1)] = lst->str[0];
}

static int	ft_calcprintstr(t_printf *lst, int *a)
{
	lst->slen = ft_strlen(lst->str);
	if (lst->prec != 0)
	{
		*a = (lst->prec > lst->slen ? lst->slen : lst->prec);
		return (*a > lst->width ? *a : lst->width);
	}
	*a = lst->slen;
	return (*a > lst->width ? *a : lst->width);
}

static void	ft_cmpstr(t_printf *lst)
{
	int		i;
	int		j;
	int		a;

	j = 0;
	i = 0;
	lst->plen = ft_calcprintstr(lst, &a);
	if (!(lst->print = ft_strcnew(lst->plen, ' ')))
		return ;
	if (lst->flag[4] && !lst->flag[1])
		while (lst->print[i])
			lst->print[i++] = '0';
	if (!lst->flag[1])
		i = lst->plen - a;
	else
		i = 0;
	while (j < a)
		lst->print[i++] = lst->str[j++];
}

static void	ft_cmpwchar(t_printf *lst)
{
	int		i;

	i = 0;
	lst->slen = ft_wchar_size(lst->wstr[0]);
	lst->plen = (lst->width > lst->slen ? lst->width - lst->slen + 1 : 1);
	if (!(lst->wprint = ft_wstrcnew(lst->plen, ' ')))
		return ;
	if (lst->flag[4] && !lst->flag[1])
		while (lst->wprint[i + 1])
			lst->wprint[i++] = '0';
	lst->wprint[(lst->flag[1] ? 0 : lst->plen - 1)] = lst->wstr[0];
}

void		ft_cmpchars(t_printf *lst)
{
	if (lst->type == 'c')
		ft_cmpchar(lst);
	else if (lst->type == 's')
		ft_cmpstr(lst);
	else if (lst->type == 'C')
		ft_cmpwchar(lst);
	else if (lst->type == 'S')
		ft_cmpwstr(lst);
}
