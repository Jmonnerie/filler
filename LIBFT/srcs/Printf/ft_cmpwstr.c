/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_cmpwstr.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/04/13 11:56:17 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/16 17:07:31 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void		ft_cmpwstr(t_printf *lst)
{
	int		i;
	int		j;

	j = 0;
	i = 0;
	lst->slen = ft_wstroctlen(lst->wstr);
	lst->plen = lst->width > lst->slen ?
		lst->width - lst->slen + ft_wstrlen(lst->wstr) : ft_wstrlen(lst->wstr);
	if (!(lst->wprint = ft_wstrcnew(lst->plen, ' ')))
		return ;
	if (lst->flag[4] && !lst->flag[1])
		while (lst->wprint[i])
			lst->wprint[i++] = '0';
	i = lst->flag[1] ? 0 : lst->plen - ft_wstrlen(lst->wstr);
	while ((size_t)j < ft_wstrlen(lst->wstr))
		lst->wprint[i++] = lst->wstr[j++];
}
