/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_parse.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/01/15 23:13:16 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/16 17:07:33 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

int		ft_parse(t_printf *lst, char *str, va_list ap)
{
	str++;
	if (*str == 0)
		return (0);
	while (*str != 0 && lst->type == 0)
	{
		if (ft_strchr("#+ -0", *str))
			ft_flag(lst, &str);
		else if ((*str <= '9' && *str >= '1') || *str == '*')
			ft_width(lst, &str, ap);
		else if (*str == '.')
			ft_precision(lst, &str, ap);
		else if (ft_strchr("hljz", *str))
			ft_length(lst, &str);
		else if (ft_strchr("XxOoUuDdiCcSspb%", *str))
		{
			lst->type = *str;
			ft_getthearg(lst, ap);
			str++;
		}
		else
			return (-1);
	}
	if (lst->error == -1)
		return (-1);
	return (lst->type);
}
