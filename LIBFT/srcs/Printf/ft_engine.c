/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_engine.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/01/15 21:33:55 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/16 17:07:32 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void		ft_free_pf(t_printf *lst)
{
	if (lst)
	{
		if (lst->str)
			free(lst->str);
		if (lst->print)
			free(lst->print);
		if (lst->wstr)
			free(lst->wstr);
		free(lst);
		lst = NULL;
	}
}

int			ft_engine(char *form, long int cha, char **str, va_list ap)
{
	int			ret;
	t_printf	*lst;

	ret = 0;
	if (!(lst = ft_struct()))
		return (-1);
	if (ft_parse(lst, *str, ap) <= 0)
		return (ft_badparse(form, cha, lst, str));
	else
	{
		ft_compute(lst);
		ret += write(1, form, cha);
		if (lst->type == 'C' && lst->wstr[0] == '\0')
			ret += write(1, lst->wstr, 1);
		ret += (lst->type == 'C' || lst->type == 'S' ?
			ft_putwstr(lst->wprint) : write(1, lst->print, lst->plen));
		*str += 1;
		if ((lst->type == 'C' || lst->type == 'S') && lst->length == 'l')
			lst->type = lst->type == 'C' ? 'c' : 's';
		while (**str != lst->type)
			*str += 1;
		ft_free_pf(lst);
		return (ret);
	}
}
