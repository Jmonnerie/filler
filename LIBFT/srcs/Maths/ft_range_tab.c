/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_range_tab.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/04/25 00:57:42 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/25 00:57:55 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

float			ft_range_tab(int ia, int ib, int ja, int jb)
{
	int			dy;
	int			dx;
	float		range;

	dy = ABS((ia - ib));
	dx = ABS((ja - jb));
	range = ft_sqrt((float)((dy * dy) + (dx * dx)));
	return (range);
}
