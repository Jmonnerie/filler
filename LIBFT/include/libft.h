/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   libft.h                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2017/12/14 19:15:04 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/25 01:03:14 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃                                     DEFINES                                ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/
# define MACOS
# ifdef MACOS
#  define UINTMAX uintmax_t
#  define INTMAX intmax_t
# endif
# ifdef LINUX
#  define UINTMAX __uintmax_t
#  define INTMAX __intmax_t
# endif

# define BUFF_SIZE		4096
# define SUCCESS		1
# define FAIL			0
# define ABS(x)			(x < 0 ? -x : x)

/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃                                     INCLUDES                               ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/
# include "colors.h"
# include <string.h>
# include <fcntl.h>
# include <stdlib.h>
# include <unistd.h>
# include <wchar.h>
# include <stdarg.h>
# include <limits.h>
# include <sys/types.h>
# include <locale.h>
# include <stdio.h>

/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃                                     STRUCTS                                ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/
typedef struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

typedef struct		s_printf
{
	int				flag[5];
	int				width;
	int				prec;
	char			length;
	char			type;
	int				neg;
	char			*str;
	wchar_t			*wstr;
	int				slen;
	char			*print;
	wchar_t			*wprint;
	int				plen;
	char			error;
}					t_printf;
/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃                                     CHARS                                  ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/
int					ft_atoi(const char *str);
char				*ft_chartostr(char c);
int					ft_tolower(int c);
int					ft_toupper(int c);
int					ft_wchar_size(wchar_t wc);
wchar_t				*ft_wchartowstr(wchar_t c);
/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃                                     GNL                                    ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/
int					get_next_line(const int fd, char **line);
/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃                                     INTS                                   ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/
void				ft_int_swap(int *a, int *b);
int					ft_intlen(int nb);
char				*ft_itoa(int n);
int					ft_lintlen(long long int nb);
char				*ft_litoa(long long int n);
char				*ft_litoa_base(long long int n, int base);
int					ft_ulintlen(unsigned long long int nb);
char				*ft_ulitoa(unsigned long long int n);
char				*ft_ulitoa_base(unsigned long long int n, int base);
/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃                                     LISTS                                  ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/
void				ft_lstadd(t_list **alst, t_list *n);
void				ft_lstdel(t_list **alst, void (*del)(void *, size_t));
void				ft_lstdelone(t_list **alst, void (*del)(void *, size_t));
void				ft_lstiter(t_list *lst, void (*f)(t_list *elem));
t_list				*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));
t_list				*ft_lstnew(void const *content, size_t content_size);
/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃                                     MATHS                                  ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/
float				ft_range_tab(int ia, int ib, int ja, int jb);
float				ft_sqrt(float number);
/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃                                     MEMORY                                 ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/
void				ft_bzero(void *s, size_t n);
void				ft_gswap(void *p1, void *p2, size_t size);
void				*ft_memalloc(size_t size);
void				*ft_memccpy(void *dst, const void *src, int c, size_t n);
void				*ft_memchr(const void *s, int c, size_t n);
int					ft_memcmp(const void *s1, const void *s2, size_t n);
void				*ft_memcpy(void *dst, const void *src, size_t n);
void				ft_memdel(void **ap);
void				*ft_memmove(void *dst, const void *src, size_t len);
void				*ft_memset(void *b, int c, size_t len);
/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃                                     PRINTF                                 ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/

int					ft_badparse(char *form, long int cha, t_printf *lst,
		char **pstr);
void				ft_cmpbin(t_printf *lst);
void				ft_cmpchars(t_printf *lst);
void				ft_cmpercent(t_printf *lst);
void				ft_cmphex(t_printf *lst);
void				ft_cmpint(t_printf *lst);
void				ft_cmpoct(t_printf *lst);
void				ft_cmpptr(t_printf *lst);
void				ft_cmpwstr(t_printf *lst);
int					ft_colors(char **str);
void				ft_compute(t_printf *lst);
int					ft_engine(char *form, long int cha, char **str, va_list ap);
void				ft_flag(t_printf *lst, char **str);
void				ft_free_pf(t_printf *lst);
void				ft_getcns(va_list ap, t_printf *lst);
void				ft_getthearg(t_printf *lst, va_list ap);
void				ft_length(t_printf *lst, char **str);
int					ft_parse(t_printf *lst, char *str, va_list ap);
void				ft_precision(t_printf *lst, char **str, va_list ap);
int					ft_print_dat_shit(t_printf *lst);
int					ft_printf(const char *restrict format, ...);
t_printf			*ft_struct(void);
void				ft_width(t_printf *lst, char **str, va_list ap);

/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃                                     PUTS                                   ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/
void				ft_putchar(char c);
void				ft_putchar_fd(char c, int fd);
void				ft_putendl(char const *s);
void				ft_putendl_fd(char const *s, int fd);
void				ft_putnbr(int n);
void				ft_putnbr_fd(int n, int fd);
void				ft_putstr(char const *s);
void				ft_putstr2d(char **str);
void				ft_putstr_fd(char const *s, int fd);
void				ft_putstrlst(t_list **alst);
int					ft_putwchar(wchar_t c);
int					ft_putwstr(wchar_t *s);
/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃                                     STRINGS                                ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/
void				ft_freestr2d(char **tab);
void				ft_freestr2dsize(char **tab, int size);
int					ft_str2dlen(char **str);
char				*ft_strcat(char *s1, const char *s2);
char				*ft_strchr(const char *s, char c);
void				ft_strclr(char *s);
int					ft_strcmp(const char *s1, const char*s2);
char				*ft_strcnew(size_t size, char c);
char				*ft_strcpy(char *dst, const char *src);
void				ft_strdel(char **as);
char				*ft_strdup(const char *s1);
int					ft_strequ(char const *s1, char const *s2);
void				ft_striter(char *s, void (*f)(char *));
void				ft_striteri(char *s, void (*f)(unsigned int, char *));
char				*ft_strjoin(char const *s1, char const *s2);
size_t				ft_strlcat(char *dst, const char *src, size_t size);
size_t				ft_strlen(const char *s);
char				*ft_strmap(char const *s, char (*f)(char));
char				*ft_strmapi(char const *s, char (*f)(unsigned int, char));
char				*ft_strncat(char *s1, const char *s2, size_t n);
int					ft_strncmp(const char *s1, const char*s2, size_t n);
char				*ft_strncpy(char *dst, const char *src, size_t len);
int					ft_strnequ(char const *s1, char const *s2, size_t n);
char				*ft_strnew(size_t size);
char				*ft_strnstr(const char *haystack,
					const char *needle, size_t len);
char				*ft_strrchr(const char *s, char c);
char				**ft_strsplit(char const *s, char c);
char				*ft_strstr(const char *haystack, const char *needle);
char				*ft_strsub(char const *s, unsigned int start, size_t len);
char				*ft_strtolower(char *str);
char				*ft_strtoupper(char *str);
char				*ft_strtrim(char const *s);
wchar_t				*ft_wstrcnew(size_t size, char c);
wchar_t				*ft_wstrdup(wchar_t *wstr);
size_t				ft_wstrlen(wchar_t *wstr);
int					ft_wstroctlen(wchar_t *wstr);
/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃                                     TESTS                                  ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/
int					ft_isalpha(int c);
int					ft_isdigit(int c);
int					ft_isalnum(int c);
int					ft_isascii(int c);
int					ft_isprint(int c);

#endif
