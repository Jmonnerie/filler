# **************************************************************************** #
#                                                           LE - /             #
#                                                               /              #
#    Makefile                                         .::    .:/ .      .::    #
#                                                  +:+:+   +:    +:  +:+:+     #
#    By: jjanin-r <jjanin-r@student.le-101.fr>      +:+   +:    +:    +:+      #
#                                                  #+#   #+    #+    #+#       #
#    Created: 2017/11/07 14:07:48 by jmonneri     #+#   ##    ##    #+#        #
#    Updated: 2018/04/25 22:57:36 by jmonneri    ###    #+. /#+    ###.fr      #
#                                                          /                   #
#                                                         /                    #
# **************************************************************************** #

.PHONY: all clean fclean re test1 test2 test3

NAME = jmonneri.filler
P1 = ./$(NAME)
P2 = ./resources/players/carli.filler
CC = gcc
CC_FLAGS = -Wall -Wextra -Werror
PATH_SRC = ./srcs/
PATH_OBJ = ./objs/
PATH_INC = ./include/
INCS = $(addprefix $(PATH_INC), filler.h)

#******************************************************************************#
#                                    FILLER                                    #
#******************************************************************************#

PATH_SRC_FILLER = $(PATH_SRC)filler/
PATH_OBJ_FILLER = $(PATH_OBJ)filler/
FILES_FILLER = $(shell ls $(PATH_SRC_FILLER) | grep '\.c' | cut -d "." -f 1)
OBJ_FILLER = $(addprefix $(PATH_OBJ_FILLER), $(addsuffix .o, $(FILES_FILLER)))
SRC_FILLER = $(addprefix $(PATH_SRC_FILLER), $(addsuffix .c, $(FILES_FILLER)))

#******************************************************************************#
#                                    LIBFT                                     #
#******************************************************************************#

PATH_LIBFT = ./LIBFT/
NAME_LIBFT = ft
LIBFT = $(PATH_LIBFT)lib$(NAME_LIBFT).a
ILIBFT = -L $(PATH_LIBFT) -l $(NAME_LIBFT)

#******************************************************************************#
#                                    ALL                                       #
#******************************************************************************#

PATHS_OBJ = $(PATH_OBJ) $(PATH_OBJ_FILLER)
OBJS = $(OBJ_FILLER)
SRCS = $(SRC_FILLER)
FILES = $(FILES_FILLER)

#******************************************************************************#
#                                    RULES                                     #
#******************************************************************************#

all: $(NAME)

clean:
	@echo "\n\033[1m🦄 🦄 🦄 🦄 SUPPRESSION DES OBJETS🦄 🦄 🦄 🦄\033[0m\n"
	@rm -rf $(PATH_OBJ)
	@make clean -C $(PATH_LIBFT)

fclean:
	@echo "\n\033[1m🦄 🦄 🦄 🦄 SUPPRESSION DE $(NAME)🦄 🦄 🦄 🦄\033[0m\n"
	@rm -f $(NAME)
	@rm -rf $(PATH_OBJ)
	@rm -f filler.trace
	@rm -f log.txt
	@make fclean -C $(PATH_LIBFT)

test1: all
	@rm -rf log.txt
	@touch log.txt
	@./resources/filler_vm -f ./resources/maps/map00 -p1 $(P1) -p2 $(P2)\
		| ./filler_viz

test2: all
	@rm -rf log.txt
	@touch log.txt
	@./resources/filler_vm -f ./resources/maps/map01 -p1 $(P1) -p2 $(P2)\
		| ./filler_viz

test3: all
	@rm -rf log.txt
	@touch log.txt
	@./resources/filler_vm -f ./resources/maps/map02 -p1 $(P1) -p2 $(P2)\
		| ./filler_viz


re: fclean all

#******************************************************************************#
#                             Compilation LIBFT                                #
#******************************************************************************#

$(NAME): $(LIBFT) $(PATHS_OBJ) $(OBJS)
	@echo "\n\033[1m🦄 🦄 🦄 🦄 CREATION DE FILLER🦄 🦄 🦄 🦄\033[0m\n"
	@$(CC) $(CC_FLAGS) $(ILIBFT) $(OBJS) -o $@
	@echo "  👍  👍  👍 \033[1mFILLER CREE\033[0m👍  👍  👍\n"

$(LIBFT):
	@make -C $(PATH_LIBFT)

$(PATHS_OBJ):
	@mkdir $@

$(PATH_OBJ)%.o: $(PATH_SRC)%.c $(INCS)
	@printf %b "0️⃣  Compilation de \033[1m$<\033[0m en \033[1m$@\033[0m..."
	@$(CC) $(CC_FLAGS) -o $@ -c $< -I $(PATH_INC)
	@printf "\r"
	@printf "                                                                                     \r"
