/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   flr_parse.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/04/17 15:34:13 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/25 21:13:41 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "filler.h"

int				flr_get_rushes(t_filler *lst)
{
	if (lst->j_beg.x < lst->tabx / 2)
	{
		lst->rush_l = flr_new_dot(lst->tabx, lst->taby / 2);
		lst->rush_r = lst->j_beg.y < lst->taby / 2 ?
			flr_new_dot(lst->tabx / 2, lst->taby) :
			flr_new_dot(lst->tabx / 2, 0);
	}
	else
	{
		lst->rush_l = flr_new_dot(0, lst->taby / 2);
		lst->rush_r = lst->j_beg.y < lst->taby / 2 ?
			flr_new_dot(lst->tabx / 2, lst->taby) :
			flr_new_dot(lst->tabx / 2, 0);
	}
	return (SUCCESS);
}

int				flr_get_begin(t_filler *lst)
{
	int			i;
	int			j;

	i = -1;
	while (++i < lst->taby)
	{
		j = -1;
		while (++j < lst->tabx)
		{
			if (lst->tab[i][j] != '.')
			{
				if (lst->tab[i][j] == lst->xo)
				{
					lst->j_beg.x = j;
					lst->j_beg.y = i;
				}
				else
				{
					lst->e_beg.x = j;
					lst->e_beg.y = i;
				}
			}
		}
	}
	return (flr_get_rushes(lst));
}
