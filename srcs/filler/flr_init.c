/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   flr_init.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/04/19 12:51:57 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/25 21:00:27 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "filler.h"

t_flrdot			flr_new_dot(int x, int y)
{
	t_flrdot		n;

	n.x = x;
	n.y = y;
	return (n);
}

int					flr_init_prt(t_filler *lst)
{
	int				i;
	int				j;

	i = -1;
	if (!(lst->prt = (float **)malloc(sizeof(float *) * lst->taby)))
		return (lst->error = 'm');
	while (++i < lst->taby)
	{
		j = -1;
		if (!(lst->prt[i] = (float *)malloc(sizeof(float) * lst->tabx)))
			return (lst->error = 'm');
		while (++j < lst->tabx)
			lst->prt[i][j] = lst->taby + lst->tabx;
	}
	return (SUCCESS);
}

int					flr_init(t_filler **lst)
{
	if (!(*lst = (t_filler *)malloc(sizeof(t_filler))))
		return (FAIL);
	(*lst)->j_beg = flr_new_dot(-1, -1);
	(*lst)->e_beg = flr_new_dot(-1, -1);
	(*lst)->j_last = flr_new_dot(0, 0);
	(*lst)->put = flr_new_dot(0, 0);
	(*lst)->rush = flr_new_dot(0, 0);
	(*lst)->rush_l = flr_new_dot(0, 0);
	(*lst)->rush_r = flr_new_dot(0, 0);
	(*lst)->tab = NULL;
	(*lst)->pcs = NULL;
	(*lst)->prt = NULL;
	(*lst)->taby = 0;
	(*lst)->tabx = 0;
	(*lst)->pcsx = 0;
	(*lst)->pcsy = 0;
	(*lst)->xo = 0;
	(*lst)->ox = 0;
	(*lst)->error = 0;
	(*lst)->left_right = 'l';
	return (SUCCESS);
}
