/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   i_feel_it.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/04/17 15:32:14 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/25 21:25:48 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "filler.h"

void				ft_freefloat2dsize(float **tab, int size)
{
	int				i;

	i = -1;
	while (++i < size)
		free(tab[i]);
	free(tab);
}

int					flr_free(t_filler *lst)
{
	int				i;

	i = -1;
	if (lst->tab)
	{
		while (++i < lst->taby)
			free(lst->tab[i] - 4);
		free(lst->tab);
	}
	if (lst->pcs)
		ft_freestr2dsize(lst->pcs, lst->pcsy);
	if (lst->prt)
		ft_freefloat2dsize(lst->prt, lst->taby);
	if (lst)
		free(lst);
	return (SUCCESS);
}

int					flr_clean_turn(t_filler *lst)
{
	int				i;

	i = -1;
	if (lst->tab)
	{
		while (++i < lst->taby)
			free(lst->tab[i] - 4);
		free(lst->tab);
		lst->tab = NULL;
	}
	if (lst->pcs)
	{
		ft_freestr2dsize(lst->pcs, lst->pcsy);
		lst->pcs = NULL;
	}
	if (lst->prt)
	{
		ft_freefloat2dsize(lst->prt, lst->taby);
		lst->prt = NULL;
	}
	return (SUCCESS);
}

int					flr_error(t_filler *lst)
{
	if (lst->error == 'e')
		ft_printf("0 0\n");
	flr_free(lst);
	return (-1);
}

int					main(void)
{
	t_filler		*lst;

	lst = NULL;
	if (!(flr_init(&lst)))
		return (flr_error(lst));
	flr_play(lst);
	return (flr_error(lst));
}
