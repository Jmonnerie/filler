/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   flr_test.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/04/19 12:47:47 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/25 21:29:07 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "filler.h"

void			flr_enemy_is_near(t_filler *lst, t_flrdot t)
{
	int			i;
	int			j;

	i = -1;
	while (++i < lst->pcsy && i + t.y + 1 < lst->taby && i + t.y - 1 > 0)
	{
		j = -1;
		while (++j < lst->pcsx && j + t.x + 1 < lst->tabx && j + t.x - 1 > 0)
		{
			if (lst->pcs[i][j] == '*')
			{
				lst->prt[t.y][t.x] +=
					(lst->tab[i + t.y + 1][j + t.x - 1] == lst->ox) * 200 +
					(lst->tab[i + t.y + 1][j + t.x] == lst->ox) * 200 +
					(lst->tab[i + t.y + 1][j + t.x + 1] == lst->ox) * 200 +
					(lst->tab[i + t.y][j + t.x - 1] == lst->ox) * 200 +
					(lst->tab[i + t.y][j + t.x + 1] == lst->ox) * 200 +
					(lst->tab[i + t.y - 1][j + t.x - 1] == lst->ox) * 200 +
					(lst->tab[i + t.y - 1][j + t.x] == lst->ox) * 200 +
					(lst->tab[i + t.y - 1][j + t.x + 1] == lst->ox) * 200;
			}
		}
	}
}

int				flr_test(t_filler *lst, t_flrdot t)
{
	int			i;
	int			j;
	int			k;
	int			l;

	l = 0;
	k = 0;
	i = -1;
	while (++i < lst->pcsy)
	{
		j = -1;
		while (++j < lst->pcsx)
		{
			if (lst->pcs[i][j] == '*' &&
				lst->tab[i + t.y][j + t.x] == lst->xo)
				k++;
			else if (lst->pcs[i][j] == '*' &&
				lst->tab[i + t.y][j + t.x] == lst->ox)
				l++;
		}
	}
	if (k == 1 && l == 0)
		flr_enemy_is_near(lst, t);
	return (k == 1 && l == 0 ? SUCCESS : FAIL);
}
