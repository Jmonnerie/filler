/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   flr_proba.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/04/19 11:37:04 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/25 21:02:12 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "filler.h"

int				flr_rush_it(t_filler *lst)
{
	int			i;
	int			j;
	float		init;

	init = lst->tabx + lst->taby;
	i = -1;
	while (++i < lst->taby)
	{
		j = -1;
		while (++j < lst->tabx)
		{
			if (lst->prt[i][j] > init)
				lst->prt[i][j] -= ft_range_tab(i, lst->rush.y, j, lst->rush.x);
			else
				lst->prt[i][j] = 0;
		}
	}
	return (SUCCESS);
}

int				flr_alg_it(t_filler *lst)
{
	lst->rush = lst->left_right == 'l' ? lst->rush_l : lst->rush_r;
	lst->left_right = lst->left_right == 'l' ? 'r' : 'l';
	flr_rush_it(lst);
	return (SUCCESS);
}

int				flr_calc_proba(t_filler *lst)
{
	t_flrdot	t;

	t.y = 0;
	while (t.y + lst->pcsy < lst->taby + 1)
	{
		t.x = 0;
		while (t.x + lst->pcsx < lst->tabx + 1)
		{
			if (flr_test(lst, t))
				lst->prt[t.y][t.x] += 1;
			t.x += 1;
		}
		t.y += 1;
	}
	flr_alg_it(lst);
	return (SUCCESS);
}
