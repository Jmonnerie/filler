/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   flr_read.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/04/17 15:45:00 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/25 21:24:35 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "filler.h"

static void	flr_tray_size(char *line, t_filler *lst)
{
	int		i;

	i = 0;
	while (line[i++] != ' ')
		;
	lst->taby = ft_atoi(line + i);
	while (ft_isdigit(line[i++]))
		;
	lst->tabx = ft_atoi(line + i);
	ft_strdel(&line);
}

int			flr_first_read(t_filler *lst)
{
	char	*line;

	line = NULL;
	if (get_next_line(0, &line) == -1)
		return (lst->error = 'g');
	lst->xo = (ft_strstr(line, "exec p1") != 0 ? 'O' : 'X');
	lst->ox = (lst->xo == 'X' ? 'O' : 'X');
	ft_strdel(&line);
	if (get_next_line(0, &line) == -1)
		return (lst->error = 'g');
	flr_tray_size(line, lst);
	return (1);
}

int			flr_read_tab(t_filler *lst)
{
	char	*line;
	char	**tab;
	int		i;

	i = 0;
	line = NULL;
	if (!(get_next_line(0, &line)))
		return (lst->error = 'g');
	if (line[0] == 'P')
	{
		ft_strdel(&line);
		if (!(get_next_line(0, &line)))
			return (lst->error = 'g');
	}
	ft_strdel(&line);
	if (!(tab = (char **)malloc(sizeof(char *) * lst->taby)))
		return (lst->error = 'm');
	while (i < lst->taby)
	{
		if (!(get_next_line(0, &line)))
			return (lst->error = 'g');
		tab[i++] = line + 4;
	}
	lst->tab = tab;
	return (lst->j_beg.x == -1 && lst->j_beg.y == -1 ? flr_get_begin(lst) : 1);
}

static void	flr_pcs_size(char *line, t_filler *lst)
{
	int		i;

	i = 0;
	while (line[i++] != ' ')
		;
	lst->pcsy = ft_atoi(line + i);
	while (ft_isdigit(line[i++]))
		;
	lst->pcsx = ft_atoi(line + i);
}

int			flr_read_pcs(t_filler *lst)
{
	char	*line;
	char	**pcs;
	int		i;

	i = 0;
	line = NULL;
	if (!(get_next_line(0, &line)))
		return (lst->error = 'g');
	flr_pcs_size(line, lst);
	ft_strdel(&line);
	if (!(pcs = (char **)malloc(sizeof(char *) * lst->pcsy)))
		return (lst->error = 'm');
	while (i < lst->pcsy)
	{
		if (!(get_next_line(0, &line)))
			return (lst->error = 'g');
		pcs[i++] = line;
	}
	lst->pcs = pcs;
	return (SUCCESS);
}
