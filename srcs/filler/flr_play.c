/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   flr_play.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/04/17 15:32:38 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/25 20:52:47 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "filler.h"

int				flr_take_the_good_point(t_filler *lst)
{
	int			i;
	int			j;
	float		big;

	big = 0;
	i = -1;
	while (++i < lst->taby)
	{
		j = -1;
		while (++j < lst->tabx)
		{
			if (lst->prt[i][j] > big)
			{
				big = lst->prt[i][j];
				lst->put.x = j;
				lst->put.y = i;
			}
		}
	}
	return (big == 0 ? FAIL : SUCCESS);
}

int				flr_compute(t_filler *lst)
{
	if (flr_init_prt(lst) != SUCCESS)
		return (FAIL);
	flr_calc_proba(lst);
	if (!(flr_take_the_good_point(lst)))
		lst->error = 'e';
	else
		ft_printf("%d %d\n", lst->put.y, lst->put.x);
	lst->j_last = lst->put;
	lst->put = flr_new_dot(0, 0);
	return (SUCCESS);
}

int				flr_play(t_filler *lst)
{
	if (flr_first_read(lst) != 1)
		return (FAIL);
	while (lst->error == 0)
	{
		if (flr_read_tab(lst) != 1)
			return (FAIL);
		if (flr_read_pcs(lst) != 1)
			return (FAIL);
		flr_compute(lst);
		flr_clean_turn(lst);
	}
	return (SUCCESS);
}
